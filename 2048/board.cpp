#include "board.h"

#include <cstring>
#include <ctime>
#include <cstdlib>

Board::Board(int size){
    srand (time(NULL));

    this->size = size;
    buffer = new int[size*size];
    backupBuffer = new int[size*size];
    memset(buffer, 0, size*size * sizeof(int));
    score = 0;
    addElement();
    addElement();

    memcpy(backupBuffer, buffer, size*size * sizeof(int));
    oldscore = score;
}

Board::~Board(){
    delete buffer;
    delete backupBuffer;
}

void Board::doMove(direction dir){
    if (!canMoveDir(dir)) return;

    memcpy(backupBuffer, buffer, size*size * sizeof(int));
    oldscore = score;

    switch (dir) {
        case left_dir:
            //move to left before
            for (int y = 0; y < size; y++){
                for (int x = 0; x < size; x++){
                    int val = getFieldValue(x,y);
                    if (val) {
                        int i;
                        bool move = false;
                        for (i = 1; getFieldValue(x-i,y) == 0; i++) {move = true;} i--;
                        if (move){
                            setFieldValue(x-i,y,val);
                            setFieldValue(x,y,0);
                        }
                    }
                }
            }
            //combine
            for (int y = 0; y < size; y++){
                for (int x = 0; x < size; x++){
                    int val = getFieldValue(x,y);
                    if (val) {
                        if (getFieldValue(x+1,y) == val){
                            setFieldValue(x,y,val*2);
                            setFieldValue(x+1,y,0);
                            score += val*2;
                        }
                    }
                }
            }
            //move to left after
            for (int y = 0; y < size; y++){
                for (int x = 0; x < size; x++){
                    int val = getFieldValue(x,y);
                    if (val) {
                        int i;
                        bool move = false;
                        for (i = 1; getFieldValue(x-i,y) == 0; i++) {move = true;} i--;
                        if (move){
                            setFieldValue(x-i,y,val);
                            setFieldValue(x,y,0);
                        }
                    }
                }
            }
            break;
        case right_dir:
            //move to right before
            for (int y = 0; y < size; y++){
                for (int x = size -1; x >= 0; x--){
                    int val = getFieldValue(x,y);
                    if (val) {
                        int i;
                        bool move = false;
                        for (i = 1; getFieldValue(x+i,y) == 0; i++) {move = true;} i--;
                        if (move){
                            setFieldValue(x+i,y,val);
                            setFieldValue(x,y,0);
                        }
                    }
                }
            }
            //combine
            for (int y = 0; y < size; y++){
                for (int x = size-1; x >= 0; x--){
                    int val = getFieldValue(x,y);
                    if (val) {
                        if (getFieldValue(x-1,y) == val){
                            setFieldValue(x,y,val*2);
                            setFieldValue(x-1,y,0);
                            score += val*2;
                        }
                    }
                }
            }
            //move to right after
            for (int y = 0; y < size; y++){
                for (int x = size -1; x >= 0; x--){
                    int val = getFieldValue(x,y);
                    if (val) {
                        int i;
                        bool move = false;
                        for (i = 1; getFieldValue(x+i,y) == 0; i++) {move = true;} i--;
                        if (move){
                            setFieldValue(x+i,y,val);
                            setFieldValue(x,y,0);
                        }
                    }
                }
            }
            break;
        case top_dir:
            //move to top before
            for (int x = 0; x < size; x++){
                for (int y = 0; y < size; y++){
                    int val = getFieldValue(x,y);
                    if (val) {
                        int i;
                        bool move = false;
                        for (i = 1; getFieldValue(x,y-i) == 0; i++) {move = true;} i--;
                        if (move){
                            setFieldValue(x,y-i,val);
                            setFieldValue(x,y,0);
                        }
                    }
                }
            }
            //combine
            for (int x = 0; x < size; x++){
                for (int y = 0; y < size; y++){
                    int val = getFieldValue(x,y);
                    if (val) {
                        if (getFieldValue(x,y+1) == val){
                            setFieldValue(x,y,val*2);
                            setFieldValue(x,y+1,0);
                            score += val*2;
                        }
                    }
                }
            }
            //move to top after
            for (int x = 0; x < size; x++){
                for (int y = 0; y < size; y++){
                    int val = getFieldValue(x,y);
                    if (val) {
                        int i;
                        bool move = false;
                        for (i = 1; getFieldValue(x,y-i) == 0; i++) {move = true;} i--;
                        if (move){
                            setFieldValue(x,y-i,val);
                            setFieldValue(x,y,0);
                        }
                    }
                }
            }
            break;
        case bot_dir:
            //move to bot before
            for (int x = 0; x < size; x++){
                for (int y = size -1; y >= 0; y--){
                    int val = getFieldValue(x,y);
                    if (val) {
                        int i;
                        bool move = false;
                        for (i = 1; getFieldValue(x,y+i) == 0; i++) {move = true;} i--;
                        if (move){
                            setFieldValue(x,y+i,val);
                            setFieldValue(x,y,0);
                        }
                    }
                }
            }
            //combine
            for (int x = 0; x < size; x++){
                for (int y = size-1; y >= 0; y--){
                    int val = getFieldValue(x,y);
                    if (val) {
                        if (getFieldValue(x,y-1) == val){
                            setFieldValue(x,y,val*2);
                            setFieldValue(x,y-1,0);
                            score += val*2;
                        }
                    }
                }
            }
            //move to bot after
            for (int x = 0; x < size; x++){
                for (int y = size -1; y >= 0; y--){
                    int val = getFieldValue(x,y);
                    if (val) {
                        int i;
                        bool move = false;
                        for (i = 1; getFieldValue(x,y+i) == 0; i++) {move = true;} i--;
                        if (move){
                            setFieldValue(x,y+i,val);
                            setFieldValue(x,y,0);
                        }
                    }
                }
            }
            break;
    }

    addElement();
}

bool Board::canMoveDir(direction dir){
    switch (dir) {
        case left_dir:
            for (int y = 0; y < size; y++){
                bool zero = false;
                int lastval = -1;
                for (int x = 0; x < size; x++){
                    int val = getFieldValue(x,y);
                    if (lastval == val) return true;
                    if (val){
                        if (zero) return true;
                        lastval = val;
                    } else {
                        zero = true;
                    }
                }
            }
            break;
        case right_dir:
            for (int y = 0; y < size; y++){
                bool zero = false;
                int lastval = -1;
                for (int x = size-1; x >= 0; x--){
                    int val = getFieldValue(x,y);
                    if (lastval == val) return true;
                    if (val){
                        if (zero) return true;
                        lastval = val;
                    } else {
                        zero = true;
                    }
                }
            }
            break;
        case top_dir:
            for (int x = 0; x < size; x++){
                bool zero = false;
                int lastval = -1;
                for (int y = 0; y < size; y++){
                    int val = getFieldValue(x,y);
                    if (lastval == val) return true;
                    if (val){
                        if (zero) return true;
                        lastval = val;
                    } else {
                        zero = true;
                    }
                }
            }
            break;
        case bot_dir:
            for (int x = 0; x < size; x++){
                bool zero = false;
                int lastval = -1;
                for (int y = size-1; y >= 0; y--){
                    int val = getFieldValue(x,y);
                    if (lastval == val) return true;
                    if (val){
                        if (zero) return true;
                        lastval = val;
                    } else {
                        zero = true;
                    }
                }
            }
            break;
    }
    return false;
}

void Board::addElement(){
    if (!freeSpace()){
        int pos;
        for (;;){
            pos = rand() % 16;
            if (buffer[pos] == 0){
                buffer[pos] = 2;
                break;
            }
        }
    }
}

void Board::returnMove(){
    int * tmp_buff = buffer;
    buffer = backupBuffer;
    backupBuffer = tmp_buff;
    int tmp_score = score;
    score = oldscore;
    oldscore = tmp_score;
}

bool Board::freeSpace(){
    for (int i = 0; i < size*size; i++){
        if (buffer[i] == 0) return false;
    }
    return true;
}

bool Board::endGame(){
    if (canMoveDir(top_dir) || canMoveDir(bot_dir) ||
            canMoveDir(left_dir) || canMoveDir(right_dir)){
        return false;
    }
    return true;
}

int Board::getSize(){
    return size;
}

int Board::getFieldValue(int x, int y){
    if (x>=size || x < 0 || y >= size || y < 0) return -1;
    return buffer[y*size + x];
}

void Board::setFieldValue(int x, int y, int value){
    if (x>=size || x < 0 || y >= size || y < 0) return;
    buffer[y*size + x] = value;
}

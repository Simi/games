#include <iostream>
#include <iomanip>

#include "board.h"

using namespace std;

void draw(Board * board){
    cout << endl << "Score: " << board->getScore() << endl << endl;
    for (int y = 0; y < board->getSize(); y++){
        cout << "|";
        for (int x = 0; x < board->getSize(); x++){
            cout << setw(6) << board->getFieldValue(x,y) << "|";
        }
        cout << endl;
    }
}

int main()
{
    char choice;
    do {
        Board board(4);
        //infinite game loop
        for(;;){
            //draw board
            draw(&board);

            //read player input
            char dir;
            do {
                cout << endl << "Select direction (wasd), return move (r) or quit (q): ";
                cin >> dir;
            } while (!(dir == 'w' || dir == 'a' || dir == 's' || dir == 'd' || dir == 'r' || dir == 'q'));

            if (dir == 'q'){
                break;
            }

            //execute move
            switch (dir){
                case 'w':
                    board.doMove(top_dir);
                    break;
                case 'a':
                    board.doMove(left_dir);
                    break;
                case 's':
                    board.doMove(bot_dir);
                    break;
                case 'd':
                    board.doMove(right_dir);
                    break;
                case 'r':
                    board.returnMove();
                    break;
            }

            //check game end
            if(board.endGame()){
                break;
            }
        }

        draw(&board);
        cout << endl << "END GAME!!!" << endl;

        do {
            cout << endl << "New game? (yn): ";
            cin >> choice;
        } while (!(choice == 'n' || choice == 'y'));
    } while (choice == 'y'); //play again
}


#ifndef BOARD_H
#define BOARD_H

enum direction{left_dir, right_dir, top_dir, bot_dir};

class Board
{
public:
    Board(int size);
    ~Board();
    void doMove(direction dir);
    void returnMove();
    bool endGame();
    bool freeSpace();
    int getSize();
    int getFieldValue(int x, int y);
    int getScore(){return score;}
private:
    int size;
    int * buffer, * backupBuffer;
    int score, oldscore;

    void addElement();
    void setFieldValue(int x, int y, int value);
    bool canMoveDir(direction dir);
};

#endif // BOARD_H

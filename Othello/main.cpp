#include <iostream>

#include "board.h"

using namespace std;

void draw(Board * board){
    //cout << string( 100, '\n' ); //clear screen by 100 new lines

    cout << "  A B C D E F G H " << endl;
    for (int y = 0; y < board->getSize(); y++){
         cout << y+1 << "|";
         for (int x = 0; x < board->getSize(); x++){
             int value = board->getFieldValue(x,y);
             char out = ' ';
             switch (value){
                 case white:
                     out = 'X';
                     break;
                 case black:
                     out = 'O';
                     break;
                 default:
                     out = ' ';
             }
             cout << out << "|";
         }
         cout << endl;
    }
    cout << "Currently playing: " << (board->getCurrentPlayer() == white ? 'X' : 'O') << endl;
}

int main(){
    char choice;
    do {
        Board board;
        //game loop
        for(;;){
            //draw board
            draw(&board);

            //read player input
            char x,y;
            do {
                cout << endl << "Select position (a-h1-8), or quit (q): ";
                cin >> x;
                if (x == 'q'){
                    break;
                }
                cin >> y;
            } while (!(x  - 'a' >= 0 && x - 'a' < 8 && y - '1' >= 0 && y - '1' < 8));
            
            if (x == 'q'){
                break;
            }

            //execute move
            board.doMove( x - 'a', y - '1');

            //check end game
            if (board.endGame()){
                break;
            }
        }

        draw(&board);
        cout << endl << "END GAME!!!" << endl;
        cout << "Score: " << board.getScoreWhite() << " X - ";
        cout << board.getScoreBlack() << " O" << endl;

        do {
            cout << endl << "New game? (yn): ";
            cin >> choice;
        } while (!(choice == 'n' || choice == 'y'));
    } while(choice == 'y');
 
    return 0;
}

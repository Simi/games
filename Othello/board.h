#ifndef BOARD_H
#define BOARD_H

enum player { black = 1, white = 2 };

class Board
{
public:
    Board();
    ~Board();
    void doMove(int x, int y);
    bool endGame();
    int getSize();
    int getFieldValue(int x, int y);
    int getScoreWhite();
    int getScoreBlack();
    player getCurrentPlayer(){return currentPlayer;}
private:
    int size;
    int * buffer;
    player currentPlayer;

    void setFieldValue(int x, int y, player value);
    bool movePossible(int x, int y);
    player otherPlayer(){return currentPlayer == white ? black : white;}
    bool isStoneInDirection(int x, int y, int dirx, int diry, player stone);
    void setStonesInDirection(int x, int y, int dirx, int diry, player stone);
};

#endif // BOARD_H

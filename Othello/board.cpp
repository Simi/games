#include "board.h"

#include <cstring>

Board::Board(){
    size = 8;
    buffer = new int[size*size];
    memset(buffer, 0, size*size * sizeof(int));
    setFieldValue(3, 3, white);
    setFieldValue(4, 4, white);
    setFieldValue(3, 4, black);
    setFieldValue(4, 3, black);
    currentPlayer = black;
}

Board::~Board(){
    delete buffer;
}

void Board::doMove(int x, int y){
    if (x < 0 || x > 8 || y < 0 || y > 8) return;
    
    if (!movePossible(x,y)) return;
    setFieldValue(x, y, currentPlayer);

    if (getFieldValue(x-1, y-1) == otherPlayer()){
        if (isStoneInDirection(x, y, -1, -1, currentPlayer)){
            setStonesInDirection(x, y, -1, -1, currentPlayer);
        }
    }
    if (getFieldValue(x-1, y) == otherPlayer()){
        if (isStoneInDirection(x, y, -1, 0, currentPlayer)){
            setStonesInDirection(x, y, -1, 0, currentPlayer);
        }
    }
    if (getFieldValue(x-1, y+1) == otherPlayer()){
        if (isStoneInDirection(x, y, -1, 1, currentPlayer)){
            setStonesInDirection(x, y, -1, 1, currentPlayer);
        }
    }
    if (getFieldValue(x, y-1) == otherPlayer()){
        if (isStoneInDirection(x, y, 0, -1, currentPlayer)){
            setStonesInDirection(x, y, 0, -1, currentPlayer);
        }
    }
    if (getFieldValue(x, y+1) == otherPlayer()){
        if (isStoneInDirection(x, y, 0, 1, currentPlayer)){
            setStonesInDirection(x, y, 0, 1, currentPlayer);
        }
    }
    if (getFieldValue(x+1, y-1) == otherPlayer()){
        if (isStoneInDirection(x, y, 1, -1, currentPlayer)){
            setStonesInDirection(x, y, 1, -1, currentPlayer);
        }
    }
    if (getFieldValue(x+1, y) == otherPlayer()){
        if (isStoneInDirection(x, y, 1, 0, currentPlayer)){
            setStonesInDirection(x, y, 1, 0, currentPlayer);
        }
    }
    if (getFieldValue(x+1, y+1) == otherPlayer()){
        if (isStoneInDirection(x, y, 1, 1, currentPlayer)){
            setStonesInDirection(x, y, 1, 1, currentPlayer);
        }
    }


    currentPlayer = otherPlayer();
}

void Board::setStonesInDirection(int x, int y, int dirx, int diry, player stone){
    for (int i = 1; i < 8; i++) {
        if (getFieldValue(x + i * dirx, y + i * diry) == stone) break;
        if (getFieldValue(x + i * dirx, y + i * diry) == 0) break;
        if (getFieldValue(x + i * dirx, y + i * diry) == otherPlayer()) setFieldValue(x + i * dirx, y + i * diry, currentPlayer);
    }
}

bool Board::movePossible(int x, int y){
    if (getFieldValue(x, y) != 0) return false;

    if (getFieldValue(x-1, y-1) == otherPlayer()){ 
        if (isStoneInDirection(x, y, -1, -1, currentPlayer)) return true; 
    }
    if (getFieldValue(x-1, y) == otherPlayer()){
        if (isStoneInDirection(x, y, -1, 0, currentPlayer)) return true;
    }
    if (getFieldValue(x-1, y+1) == otherPlayer()){
        if (isStoneInDirection(x, y, -1, 1, currentPlayer)) return true;
    }
    if (getFieldValue(x, y-1) == otherPlayer()){
        if (isStoneInDirection(x, y, 0, -1, currentPlayer)) return true;
    }
    if (getFieldValue(x, y+1) == otherPlayer()){
        if (isStoneInDirection(x, y, 0, 1, currentPlayer)) return true;
    }
    if (getFieldValue(x+1, y-1) == otherPlayer()){
        if (isStoneInDirection(x, y, 1, -1, currentPlayer)) return true;
    }
    if (getFieldValue(x+1, y) == otherPlayer()){
        if (isStoneInDirection(x, y, 1, 0, currentPlayer)) return true;
    }
    if (getFieldValue(x+1, y+1) == otherPlayer()){
        if (isStoneInDirection(x, y, 1, 1, currentPlayer)) return true;
    }

    return false;
}

bool Board::isStoneInDirection(int x, int y, int dirx, int diry, player stone){
    for (int i = 1; i < 8; i++) {
        if (getFieldValue(x + i * dirx, y + i * diry) == stone) return true;
        if (getFieldValue(x + i * dirx, y + i * diry) == 0) return false;
    }
    return false;
}

bool Board::endGame(){
    for (int i = 0; i < size * size; i++){
        if (buffer[i] == 0) return false;
    }
    
    return true;
}

int Board::getSize(){
    return size;
}

int Board::getFieldValue(int x, int y){
    if (x>=size || x < 0 || y >= size || y < 0) return -1;
    return buffer[y*size + x];
}

int Board::getScoreWhite(){
    int score;

    for (int i = 0; i < size * size; i++){
        if (buffer[i] == white) score++;
    }

    return score;
}

int Board::getScoreBlack(){
   int score;

    for (int i = 0; i < size * size; i++){
        if (buffer[i] == black) score++;
    }

    return score;
}

void Board::setFieldValue(int x, int y, player value){
    if (x>=size || x < 0 || y >= size || y < 0) return;
    buffer[y*size + x] = value;
}

